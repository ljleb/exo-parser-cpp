.PHONY: release debug

include make/options.mk
include make/tree.mk

release: $(OUT_DIRECTORY)/release/main
debug: $(OUT_DIRECTORY)/debug/main

$(OUT_DIRECTORY)/%/main: $(OUT_DIRECTORY)/%/main.o
	$(CXX) -static $(CXX_$*_OPTIONS) -o $@ $^

$(OUT_DIRECTORY)/%/main.o: $(CPP_SRC_DIRECTORY)/main.cpp $(HEADERS) | $(OUT_DIRECTORY)/%/
	$(CXX) -c $(CXX_$*_OPTIONS) -o $@ $<

$(OUT_DIRECTORY)/release/%.txt.o: $(CPP_SRC_DIRECTORY)/compiler/text/% | $(OUT_DIRECTORY)/release/
	objcopy --input binary --output elf64-x86-64 --binary-architecture i386 $< $@

$(OUT_DIRECTORY)/debug/%.txt.o: $(CPP_SRC_DIRECTORY)/compiler/text/% | $(OUT_DIRECTORY)/debug/
	objcopy --input binary --output elf64-x86-64 --binary-architecture i386 $< $@

$(OUT_DIRECTORY)/%/:
	mkdir -p $@
