.PHONY: install uninstall

include make/tree.mk

PREFIX := /usr/local

install: $(PREFIX)/bin/xo

uninstall:
	rm -f $(PREFIX)/bin/xo

$(PREFIX)/bin/xo: $(OUT_DIRECTORY)/release/main
	cp -f $^ $@
