#ifndef PARSER_HPP
#define PARSER_HPP

#include <utils/storable.hpp>
#include <neither/either.hpp>
#include <string_view>
#include <string>

namespace exo {
    struct Source final {
        Source(std::string_view const& source):
            _source { source } {}

        bool not_empty() const& {
            return !_source.empty();
        }

        bool starts_with(std::string_view const& string) const& {
            return _source.rfind(string, 0) == 0;
        }

        template <typename Condition>
        std::string_view::size_type length_of_segment_where(Condition test) const& {
            std::string_view::size_type index { 0 };
            for (; index < _source.size() && test(_source[index]); ++index);
            return index;
        }

        std::string_view segment(std::string_view::size_type const& size) const& {
            return _source.substr(0, size);
        }

        Source advance(std::string_view::size_type const& size) const& {
            return Source { _source.substr(size) };
        }

    private:
        std::string_view _source;
    };

    struct ParserInput final {
        Source source;
    };

    template<typename _Ast>
    struct Parser {
        using Ast = _Ast;

        using Input = ParserInput;

        struct Output {
            Source remainder;
            [[no_unique_address]] Storable<Ast> ast;
        };

        using PartialOutput = neither::Either<std::string, Output>;

        virtual PartialOutput try_parse(Input const& input) const& = 0;
    };
}

#endif
