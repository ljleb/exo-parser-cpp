#ifndef END_OF_INPUT_PARSER_HPP
#define END_OF_INPUT_PARSER_HPP

#include <parser.hpp>

#include <string_view>
#include <string>

namespace exo {
    struct EndOfInputParser final : public Parser<void> {
        EndOfInputParser() {}

        virtual PartialOutput try_parse(Input const& input) const& final override {
            if (input.source.not_empty()) {
                return PartialOutput::leftOf("expected to have reached end of input");
            }

            return PartialOutput::rightOf(Output {
                remainder: input.source,
            });
        }
    } const end_of_input_parser;
}

#endif
