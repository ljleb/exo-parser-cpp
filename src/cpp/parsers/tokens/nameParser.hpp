#ifndef NAME_PARSER_HPP
#define NAME_PARSER_HPP

#include "spaceParser.hpp"

#include <parsers/combinators/thenParser.hpp>
#include <parsers/combinators/takeWhileParser.hpp>

#include <cctype>

namespace exo {
    namespace NameParser {
        TakeWhileParser const alpha_parser {
            [](unsigned char character) { return std::isalpha(character); },
            1
        };
    }

    ThenParser const name_parser { NameParser::alpha_parser, space_parser };
}

#endif
