#ifndef SKIP_PARSER_HPP
#define SKIP_PARSER_HPP

#include "spaceParser.hpp"

#include <parser.hpp>

#include <string_view>
#include <string>

namespace exo {
    template <bool _skip_space = true>
    struct SkipParser final : public Parser<void> {
        SkipParser(std::string_view const& to_skip):
            _to_skip { to_skip }
        {}

        virtual PartialOutput try_parse(Input const& input) const& final override {
            if (!input.source.starts_with(_to_skip)) {
                return PartialOutput::leftOf("expected '" + std::string { _to_skip } + "'");
            }

            Input const& remainder { input.source.advance(_to_skip.size()) };

            if constexpr (_skip_space) {
                return space_parser.try_parse(remainder);
            } else {
                return PartialOutput::rightOf(Output { remainder: remainder.source });
            }
        }

    private:
        std::string_view _to_skip;
    };
}

#endif
