#ifndef SPACE_PARSER_HPP
#define SPACE_PARSER_HPP

#include <parsers/combinators/skipWhileParser.hpp>

#include <cctype>

namespace exo {
    SkipWhileParser const space_parser {
        [](unsigned char character) { return std::isspace(character); }
    };
}

#endif
