#ifndef EXO_SYMBOLIC_PROPERTY_PARSER_HPP
#define EXO_SYMBOLIC_PROPERTY_PARSER_HPP

#include <ast/properties/symbolicPropertyAst.hpp>

#include <parsers/tokens/nameParser.hpp>
#include <parsers/tokens/skipParser.hpp>

#include <parser.hpp>

namespace exo {
    extern Parser<std::unique_ptr<Ast const>> const& expression_parser;

    namespace SymbolicPropertyParsers {
        SkipParser const assignment_parser { "=" };
        ThenParser const symbolic_property_tuple_parser {
            name_parser,
            assignment_parser,
            expression_parser
        };

        MapParser const impl {
            symbolic_property_tuple_parser,
            MapParserHelpers::make_unique_mapper<SymbolicPropertyAst>(),
        };
    }

    using SymbolicPropertyParser = Parser<std::unique_ptr<Ast const>>;

    SymbolicPropertyParser const& symbolic_property_parser { SymbolicPropertyParsers::impl };
}

#endif
