#ifndef EXO_PARAMETRIC_PROPERTY_PARSER_HPP
#define EXO_PARAMETRIC_PROPERTY_PARSER_HPP

#include <ast/properties/parametricPropertyAst.hpp>

#include <parsers/tokens/nameParser.hpp>
#include <parsers/tokens/skipParser.hpp>

#include <parser.hpp>

namespace exo {
    extern Parser<std::unique_ptr<Ast const>> const& expression_parser;

    namespace ParametricPropertyParsers {
        SkipParser const parameter_begin_parser { "(" };
        SkipParser const parameter_end_parser { ")" };
        ThenParser const parameter_name_parser {
            parameter_begin_parser,
            name_parser,
            parameter_end_parser
        };

        SkipParser const assignment_parser { "=" };
        ThenParser const parametric_property_tuple_parser {
            parameter_name_parser,
            assignment_parser,
            expression_parser
        };

        MapParser const impl {
            parametric_property_tuple_parser,
            MapParserHelpers::make_unique_mapper<ParametricPropertyAst>(),
        };
    }

    using ParametricPropertyParser = Parser<std::unique_ptr<Ast const>>;

    ParametricPropertyParser const& parametric_property_parser { ParametricPropertyParsers::impl };
}

#endif
