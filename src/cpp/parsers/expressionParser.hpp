#ifndef EXO_EXPRESSION_PARSER_HPP
#define EXO_EXPRESSION_PARSER_HPP

#include <parsers/expressions/selectionExpressionParser.hpp>
#include <parsers/expressions/symbolExpressionParser.hpp>
#include <parsers/expressions/objectExpressionParser.hpp>
#include <parsers/expressions/stringLiteralExpressionParser.hpp>
#include <parsers/expressions/printExpressionParser.hpp>
#include <parsers/expressions/printErrorExpressionParser.hpp>
#include <parsers/expressions/cliArgsExpressionParser.hpp>
#include <parsers/propertyParser.hpp> // this is a bit awkward >_> (should not need this line)
#include <parsers/selectorParser.hpp> // this too

#include <parsers/tokens/endOfInputParser.hpp>
#include <parsers/tokens/spaceParser.hpp>
#include <parsers/combinators/orParser.hpp>
#include <parsers/combinators/thenParser.hpp>

namespace exo {
    namespace ExpressionParsers {
        OrParser const atom_impl {
            symbol_expression_parser,
            object_expression_parser,
            string_literal_expression_parser,
            print_expression_parser,
            print_error_expression_parser,
            cli_args_expression_parser,
        };

        OrParser const impl { selection_expression_parser, atom_impl };
        ThenParser const root_impl { space_parser, impl, end_of_input_parser };
    };

    using ExpressionParser = Parser<std::unique_ptr<Ast const>>;

    ExpressionParser const& atom_expression_parser { ExpressionParsers::atom_impl };
    ExpressionParser const& expression_parser { ExpressionParsers::impl };
    ExpressionParser const& root_expression_parser { ExpressionParsers::root_impl };
}

#endif
