#ifndef EXO_PROPERTY_PARSER_HPP
#define EXO_PROPERTY_PARSER_HPP

#include <parsers/properties/parametricPropertyParser.hpp>
#include <parsers/properties/symbolicPropertyParser.hpp>

#include <parsers/combinators/orParser.hpp>

namespace exo {
    namespace PropertyParsers {
        OrParser impl { symbolic_property_parser, parametric_property_parser };
    };

    using PropertyParser = Parser<std::unique_ptr<Ast const>>;

    PropertyParser const& property_parser { PropertyParsers::impl };
}

#endif
