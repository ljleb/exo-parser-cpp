#ifndef EXO_SYMBOL_EXPRESSION_PARSER_HPP
#define EXO_SYMBOL_EXPRESSION_PARSER_HPP

#include <ast/expressions/symbolExpressionAst.hpp>

#include <parsers/combinators/mapParser.hpp>
#include <parsers/tokens/nameParser.hpp>

namespace exo {
    MapParser const symbol_expression_parser {
        name_parser,
        MapParserHelpers::make_unique_mapper<SymbolExpressionAst>(),
    };

    using SymbolExpressionParser = decltype(symbol_expression_parser);
}

#endif
