#ifndef EXO_STRING_LITERAL_EXPRESSION_PARSER_HPP
#define EXO_STRING_LITERAL_EXPRESSION_PARSER_HPP

#include <ast/expressions/stringLiteralExpressionAst.hpp>

#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/takeWhileParser.hpp>
#include <parsers/combinators/mapParser.hpp>
#include <parsers/combinators/thenParser.hpp>

#include <parser.hpp>

namespace exo {
    namespace StringLiteralExpressionParsers {
        auto const& escape_character { '\\' };
        auto const& quote_character { '\'' };

        struct RawContentCondition final {
            template <typename _Char>
            bool operator()(_Char const& character) {
                bool const escape_mode { _next_escape_mode };
                _next_escape_mode = !_next_escape_mode && character == escape_character;
                return escape_mode || character != quote_character;
            }

        private:
            bool _next_escape_mode { false };
        };

        char const quote_string[] { quote_character, '\0' };
        SkipParser<false> const begin_quote_parser { quote_string };
        SkipParser<true> const end_quote_parser { quote_string };
        TakeWhileParser const raw_content_parser { RawContentCondition {} };
        MapParser const content_parser {
            raw_content_parser,
            [](auto const& raw_content) {
                std::string formatted_content;
                for (std::string::size_type i = 0; i < raw_content.size(); ++i) {
                    if (raw_content[i] == escape_character) ++i;
                    formatted_content += raw_content[i];
                }
                return formatted_content;
            }
        };
        ThenParser const string_literal_expression_parser { begin_quote_parser, content_parser, end_quote_parser };

        MapParser const impl {
            string_literal_expression_parser,
            MapParserHelpers::make_unique_mapper<StringLiteralExpressionAst>(),
        };
    }

    using StringLiteralExpressionParser = Parser<std::unique_ptr<Ast const>>;

    StringLiteralExpressionParser const& string_literal_expression_parser { StringLiteralExpressionParsers::impl };
}

#endif
