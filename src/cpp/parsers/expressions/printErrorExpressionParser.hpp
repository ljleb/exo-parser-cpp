#ifndef EXO_PRINT_ERROR_EXPRESSION_PARSER_HPP
#define EXO_PRINT_ERROR_EXPRESSION_PARSER_HPP

#include <ast/expressions/printExpressionAst.hpp>

#include <parsers/tokens/skipParser.hpp>

#include <parsers/combinators/mapParser.hpp>
#include <parsers/combinators/thenParser.hpp>

#include <parser.hpp>

namespace exo {
    extern Parser<std::unique_ptr<Ast const>> const& atom_expression_parser;

    namespace PrintErrorExpressionParsers {
        SkipParser const print_token_parser { "_builtin_print_error" };
        ThenParser const print_expression_tuple_parser { print_token_parser, atom_expression_parser };

        MapParser const impl {
            print_expression_tuple_parser,
            MapParserHelpers::make_unique_mapper<PrintExpressionAst<PrintStream::ERROR>>(),
        };
    }

    using PrintErrorExpressionParser = Parser<std::unique_ptr<Ast const>>;

    PrintErrorExpressionParser const& print_error_expression_parser { PrintErrorExpressionParsers::impl };
}

#endif
