#ifndef EXO_OBJECT_EXPRESSION_PARSER_HPP
#define EXO_OBJECT_EXPRESSION_PARSER_HPP

#include <ast/expressions/objectExpressionAst.hpp>

#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/notParser.hpp>
#include <parsers/combinators/mapParser.hpp>
#include <parsers/combinators/manyParser.hpp>
#include <parsers/combinators/thenParser.hpp>

#include <parser.hpp>

namespace exo {
    extern Parser<std::unique_ptr<Ast const>> const& property_parser;

    namespace ObjectExpressionParsers {
        SkipParser const object_start_parser { "{" };
        SkipParser const object_end_parser { "}" };

        NotParser const not_object_end_parser { object_end_parser };
        ThenParser const better_property_parser { not_object_end_parser, property_parser };
        ManyParser const many_properties_parser { better_property_parser };

        ThenParser const object_expression_tuple_parser {
            object_start_parser,
            many_properties_parser,
            object_end_parser
        };

        MapParser const impl {
            object_expression_tuple_parser,
            MapParserHelpers::make_unique_mapper<ObjectExpressionAst>(),
        };
    }

    using ObjectExpressionParser = Parser<std::unique_ptr<Ast const>>;
    
    ObjectExpressionParser const& object_expression_parser { ObjectExpressionParsers::impl };
}

#endif
