#ifndef EXO_SELECTION_EXPRESSION_PARSER_HPP
#define EXO_SELECTION_EXPRESSION_PARSER_HPP

#include <ast/expressions/selectionExpressionAst.hpp>

#include <parsers/tokens/nameParser.hpp>
#include <parsers/tokens/skipParser.hpp>

#include <parsers/combinators/mapParser.hpp>
#include <parsers/combinators/manyParser.hpp>
#include <parsers/combinators/thenParser.hpp>

#include <parser.hpp>

namespace exo {
    extern Parser<std::unique_ptr<Ast const>> const& atom_expression_parser;
    extern Parser<std::unique_ptr<Ast const>> const& selector_parser;

    namespace SelectionExpressionParsers {
        SkipParser const selection_token_parser { "." };
        ThenParser const chain_selector_parser { selection_token_parser, selector_parser };

        ManyParser const many_selectors_parser { chain_selector_parser, 1 };
        ThenParser const selection_expression_tuple_parser { atom_expression_parser, many_selectors_parser };

        MapParser const impl {
            selection_expression_tuple_parser,
            MapParserHelpers::make_unique_mapper<SelectionExpressionAst>(),
        };
    }

    using SelectionExpressionParser = Parser<std::unique_ptr<Ast const>>;

    SelectionExpressionParser const& selection_expression_parser { SelectionExpressionParsers::impl };
}

#endif
