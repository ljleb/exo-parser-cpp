#ifndef EXO_CLI_ARGS_EXPRESSION_PARSER_HPP
#define EXO_CLI_ARGS_EXPRESSION_PARSER_HPP

#include <ast/expressions/cliArgsExpressionAst.hpp>

#include <parsers/tokens/skipParser.hpp>

#include <parsers/combinators/mapParser.hpp>

#include <parser.hpp>

namespace exo {
    namespace CliArgsExpressionParsers {
        SkipParser const cli_args_token_parser { "_builtin_cli_args" };

        MapParser const impl {
            cli_args_token_parser,
            MapParserHelpers::make_unique_mapper<CliArgsExpressionAst>(),
        };
    }

    using CliArgsExpressionParser = Parser<std::unique_ptr<Ast const>>;

    CliArgsExpressionParser const& cli_args_expression_parser = CliArgsExpressionParsers::impl;
}

#endif
