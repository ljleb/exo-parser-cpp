#ifndef EXO_PARAMETRIC_SELECTOR_PARSER_HPP
#define EXO_PARAMETRIC_SELECTOR_PARSER_HPP

#include <parsers/combinators/thenParser.hpp>
#include <parsers/tokens/skipParser.hpp>

#include <ast/selectors/parametricSelectorAst.hpp>

#include <parser.hpp>

namespace exo {
    extern Parser<std::unique_ptr<Ast const>> const& expression_parser;

    namespace ParametricSelectorParsers {
        SkipParser const argument_begin_parser { "(" };
        SkipParser const argument_end_parser { ")" };
        ThenParser const argument_parser { argument_begin_parser, expression_parser, argument_end_parser };
    }

    MapParser const parametric_selector_parser {
        ParametricSelectorParsers::argument_parser,
        MapParserHelpers::make_unique_mapper<ParametricSelectorAst>(),
    };

    using ParametricSelectorParser = decltype(parametric_selector_parser);
}

#endif
