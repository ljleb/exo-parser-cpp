#ifndef EXO_SYMBOLIC_SELECTOR_PARSER_HPP
#define EXO_SYMBOLIC_SELECTOR_PARSER_HPP

#include <ast/selectors/symbolicSelectorAst.hpp>
#include <parsers/tokens/nameParser.hpp>

namespace exo {
    MapParser const symbolic_selector_parser {
        name_parser,
        MapParserHelpers::make_unique_mapper<SymbolicSelectorAst>(),
    };

    using SymbolicSelectorParser = decltype(symbolic_selector_parser);
}

#endif
