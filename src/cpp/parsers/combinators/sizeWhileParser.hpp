#ifndef EXO_COMBINATOR_SIZE_WHILE_PARSER_HPP
#define EXO_COMBINATOR_SIZE_WHILE_PARSER_HPP

#include <parser.hpp>

#include <string_view>
#include <string>

namespace exo {
    template <typename _Condition>
    struct SizeWhileParser final : public Parser<std::string_view::size_type> {
        SizeWhileParser(_Condition const& condition, std::string_view::size_type const& min_length = 0):
            _condition { condition },
            _min_length { min_length } {}

        virtual PartialOutput try_parse(Input const& input) const& final override {
            std::string_view::size_type const& segment_length {
                input.source.length_of_segment_where(_condition)
            };

            if (segment_length < _min_length) {
                return PartialOutput::leftOf("expected to match at least " + std::to_string(_min_length) + " characters");
            }

            return PartialOutput::rightOf(Output {
                remainder: std::move(input.source),
                ast: std::move(segment_length),
            });
        }

    private:
        _Condition const& _condition;
        std::string_view::size_type _min_length;
    };
}

#endif
