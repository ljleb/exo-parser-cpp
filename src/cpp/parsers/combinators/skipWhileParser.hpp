#ifndef EXO_COMBINATOR_SKIP_WHILE_PARSER_HPP
#define EXO_COMBINATOR_SKIP_WHILE_PARSER_HPP

#include "sizeWhileParser.hpp"

#include <parser.hpp>

#include <string_view>

namespace exo {
    template <typename _Condition>
    struct SkipWhileParser final : public Parser<void> {
        SkipWhileParser(_Condition const& condition, std::string_view::size_type const& min_size = 0):
            _size_while_parser { condition, min_size } {}

        virtual PartialOutput try_parse(Input const& input) const& final override {
            auto const&& size_output { _size_while_parser.try_parse(input) };
            if (size_output.isLeft) return PartialOutput::leftOf(size_output.leftValue);

            return PartialOutput::rightOf(Output {
                remainder: input.source.advance(size_output.rightValue.ast),
            });
        }

    private:
        SizeWhileParser<_Condition> _size_while_parser;
    };
}

#endif
