#ifndef EXO_COMBINATOR_OR_PARSER_HPP
#define EXO_COMBINATOR_OR_PARSER_HPP

#include <parser.hpp>

#include <type_traits>
#include <cstdint>

namespace exo {
    template <typename... _Parsers>
    struct OrParser final : public Parser<std::common_type_t<typename _Parsers::Ast...>> {
        OrParser(_Parsers const&... parsers):
            _parsers { parsers... } {}

        virtual typename OrParser::PartialOutput try_parse(typename OrParser::Input const& input) const& final override {
            return try_parse_recursive<0>(input);
        }

    private:
        std::tuple<_Parsers const&...> _parsers;

        template <uint64_t index>
        typename OrParser::PartialOutput try_parse_recursive(typename OrParser::Input const& input) const& {
            auto&& partial_output { std::get<index>(_parsers).try_parse(input) };

            if constexpr (index + 1 < sizeof...(_Parsers)) {
                if (partial_output.isLeft) {
                    return try_parse_recursive<index + 1>(input);
                }
            }

            return partial_output;
        }
    };
}

#endif
