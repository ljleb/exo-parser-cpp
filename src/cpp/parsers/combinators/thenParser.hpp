#ifndef EXO_COMBINATOR_THEN_PARSER_HPP
#define EXO_COMBINATOR_THEN_PARSER_HPP

#include <parser.hpp>

#include <combiner.hpp>

#include <type_traits>
#include <tuple>
#include <cstdint>

namespace exo {
    namespace ThenParserHelpers {
        template <typename LastCombiner, typename... Asts>
        struct CombinerForStruct;

        template <typename LastCombiner, typename NextAst, typename... Asts>
        struct CombinerForStruct<LastCombiner, NextAst, Asts...> {
            using Result = typename CombinerForStruct<Combiner<typename LastCombiner::Ast, NextAst>, Asts...>::Result;
        };

        template <typename LastCombiner>
        struct CombinerForStruct<LastCombiner> {
            using Result = LastCombiner;
        };

        template <typename... Asts>
        using CombinerFor = typename CombinerForStruct<Combiner<void, void>, Asts...>::Result;
    }

    template <typename... _Parsers>
    struct ThenParser final : public Parser<typename ThenParserHelpers::CombinerFor<typename _Parsers::Ast...>::Ast> {
        ThenParser(_Parsers const&... parsers):
            _parsers { parsers... } {}

        virtual typename ThenParser::PartialOutput try_parse(typename ThenParser::Input const& input) const& final override {
            if constexpr (sizeof...(_Parsers) > 0) {
                return try_parse_recursive<void, 0>({ remainder: input.source });
            }
            else {
                return ThenParser::PartialOutput::rightOf({});
            }
        }

    private:
        std::tuple<_Parsers const&...> _parsers;

        template <uint64_t index>
        using ParserAt = std::tuple_element_t<index, std::tuple<_Parsers...>>;

        template <typename LastAst, uint64_t index>
        typename ThenParser::PartialOutput try_parse_recursive(typename Parser<LastAst>::Output&& previous_output) const& {
            auto&& partial_output { std::get<index>(_parsers).try_parse({ previous_output.remainder }) };
            if (partial_output.isLeft) return ThenParser::PartialOutput::leftOf(partial_output.leftValue);

            Combiner<LastAst, typename ParserAt<index>::Ast> combiner;

            typename Parser<typename decltype(combiner)::Ast>::Output next_output {
                remainder: std::move(partial_output.rightValue.remainder),
                ast: combiner.combine(std::move(previous_output.ast), std::move(partial_output.rightValue.ast)),
            };

            if constexpr (index + 1 < sizeof...(_Parsers)) {
                return try_parse_recursive<typename decltype(combiner)::Ast, index + 1>(std::move(next_output));
            }
            else {
                return ThenParser::PartialOutput::rightOf(std::move(next_output));
            }
        }
    };
}

#endif
