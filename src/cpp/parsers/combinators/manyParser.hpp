#ifndef EXO_COMBINATOR_MANY_PARSER_HPP
#define EXO_COMBINATOR_MANY_PARSER_HPP

#include <parser.hpp>

#include <string>
#include <vector>
#include <cstdint>

namespace exo {
    template <typename _Parser>
    struct ManyParser final : public Parser<std::vector<typename _Parser::Ast>> {
        ManyParser(_Parser const& parser, uint64_t min_parse_count = 0):
            _parser { parser },
            _min_parse_count { min_parse_count } {}

        virtual typename ManyParser::PartialOutput try_parse(typename ManyParser::Input const& input) const& final override {
            Source remainder { input.source };
            typename ManyParser::Ast many_ast;

            while (true) {
                auto&& node { _parser.try_parse({ remainder }) };
                if (node.isLeft) break;

                remainder = std::move(node.rightValue.remainder);
                many_ast.push_back(std::move(node.rightValue.ast));
            }

            if (many_ast.size() < _min_parse_count) {
                return ManyParser::PartialOutput::leftOf("expected to match at least " + std::to_string(_min_parse_count) + " times");
            }

            return ManyParser::PartialOutput::rightOf(typename ManyParser::Output {
                remainder: std::move(remainder),
                ast: std::move(many_ast),
            });
        }

    private:
        _Parser const& _parser;
        uint64_t _min_parse_count;
    };
}

#endif
