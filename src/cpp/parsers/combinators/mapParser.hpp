#ifndef EXO_COMBINATOR_MAP_PARSER_HPP
#define EXO_COMBINATOR_MAP_PARSER_HPP

#include <combiner.hpp>

#include <parser.hpp>

#include <type_traits>
#include <utility>

namespace exo {
    namespace MapParserHelpers {
        template <typename _Ast, typename _Mapper>
        struct AstStruct {
            using Result = std::invoke_result_t<_Mapper, _Ast>;

            static constexpr Result invoke(_Ast&& ast, _Mapper const& mapper) {
                return mapper(std::move(ast));
            }
        };

        template <typename _Ast, typename _Mapper> requires(std::is_same_v<_Ast, void>)
        struct AstStruct<_Ast, _Mapper> {
            using Result = std::invoke_result_t<_Mapper>;

            static constexpr Result invoke(Storable<_Ast>&& ast, _Mapper const& mapper) {
                return mapper();
            }
        };

        template <typename... _Asts, typename _Mapper>
        struct AstStruct<MultipleAst<_Asts...>, _Mapper> {
            using Result = std::invoke_result_t<_Mapper, _Asts...>;

            static constexpr Result invoke(MultipleAst<_Asts...>&& asts, _Mapper const& mapper) {
                return invoke_impl(std::move(asts), mapper, std::make_index_sequence<sizeof...(_Asts)>());
            }

        private:
            template <std::size_t... indices>
            static constexpr Result invoke_impl(MultipleAst<_Asts...>&& asts, _Mapper const& mapper, std::index_sequence<indices...>) {
                return mapper(std::move(std::get<indices>(asts))...);
            }
        };

        template <typename _Ast, typename _Mapper>
        using FindAst = AstStruct<_Ast, _Mapper>::Result;

        template <typename _Ast>
        constexpr auto make_unique_mapper() {
            return [](auto&&... args) -> std::unique_ptr<Ast const> {
                return std::make_unique<_Ast const>(std::move(args)...);
            };
        }
    }

    template <typename _Parser, typename _Mapper>
    class MapParser final : public Parser<MapParserHelpers::FindAst<typename _Parser::Ast, _Mapper>> {
        using AstStruct = MapParserHelpers::AstStruct<typename _Parser::Ast, _Mapper>;

    public:
        MapParser(_Parser const& from_parser, _Mapper const& mapper):
            _from_parser { from_parser },
            _mapper { mapper } {}

        virtual typename MapParser::PartialOutput try_parse(typename MapParser::Input const& input) const& final override {
            auto&& partial_output { _from_parser.try_parse(input) };
            if (partial_output.isLeft) return MapParser::PartialOutput::leftOf(partial_output.leftValue);

            return MapParser::PartialOutput::rightOf(typename MapParser::Output {
                remainder: std::move(partial_output.rightValue.remainder),
                ast: AstStruct::invoke(std::move(partial_output.rightValue.ast), _mapper)
            });
        }

    private:
        _Parser const& _from_parser;
        _Mapper const& _mapper;
    };
}

#endif
