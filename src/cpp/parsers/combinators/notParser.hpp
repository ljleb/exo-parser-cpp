#ifndef EXO_COMBINATOR_NOT_PARSER_HPP
#define EXO_COMBINATOR_NOT_PARSER_HPP

#include <parser.hpp>

#include <string>
#include <type_traits>
#include <cstdint>

namespace exo {
    template <typename _Parser>
    struct NotParser final : public Parser<void> {
        NotParser(_Parser const& parser):
            _parser { parser } {}

        virtual typename NotParser::PartialOutput try_parse(typename NotParser::Input const& input) const& final override {
            auto&& output { _parser.try_parse(input) };
            if (!output.isLeft) return NotParser::PartialOutput::leftOf("expected to not match");

            return NotParser::PartialOutput::rightOf(typename NotParser::Output {
                remainder: input.source,
            });
        }

    private:
        _Parser const& _parser;
    };
}

#endif
