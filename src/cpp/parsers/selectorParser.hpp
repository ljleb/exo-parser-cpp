#ifndef EXO_SELECTOR_PARSER_HPP
#define EXO_SELECTOR_PARSER_HPP

#include <parsers/selectors/parametricSelectorParser.hpp>
#include <parsers/selectors/symbolicSelectorParser.hpp>

#include <parsers/combinators/orParser.hpp>

namespace exo {
    namespace SelectorParsers {
        OrParser impl { symbolic_selector_parser, parametric_selector_parser };
    };

    using SelectorParser = Parser<std::unique_ptr<Ast const>>;

    SelectorParser const& selector_parser { SelectorParsers::impl };
}

#endif
