#ifndef EXO_COMBINER_HPP
#define EXO_COMBINER_HPP

#include <utils/storable.hpp>

#include <tuple>
#include <utility>

namespace exo {
    template <typename... Args>
    struct MultipleAst : public std::tuple<Args...> {
        MultipleAst(Args&&... args):
            std::tuple<Args...> { std::forward<Args>(args)... } {}
    };

    template <typename LeftAst, typename RightAst>
    struct Combiner {
        using Ast = MultipleAst<LeftAst, RightAst>;

        Ast combine(LeftAst&& left_ast, RightAst&& right_ast) {
            return MultipleAst { std::move(left_ast), std::move(right_ast) };
        }
    };

    template <typename... LeftAsts, typename RightAst>
    struct Combiner<MultipleAst<LeftAsts...>, RightAst> {
        using Ast = MultipleAst<LeftAsts..., RightAst>;

        Ast combine(MultipleAst<LeftAsts...>&& left_asts, RightAst&& right_ast) {
            return combine_impl(std::move(left_asts), std::move(right_ast), std::make_index_sequence<sizeof...(LeftAsts)>());
        }

    private:
        template <std::size_t... indices>
        Ast combine_impl(MultipleAst<LeftAsts...>&& left_asts, RightAst&& right_ast, std::index_sequence<indices...>) {
            return MultipleAst { std::move(std::get<indices>(left_asts))..., std::move(right_ast) };
        }
    };

    template <typename RightAst>
    struct Combiner<void, RightAst> {
        using Ast = RightAst;

        Ast combine(Storable<void>, RightAst&& ast) {
            return ast;
        }
    };

    template <typename LeftAst>
    struct Combiner<LeftAst, void> {
        using Ast = LeftAst;

        Ast combine(LeftAst&& ast, Storable<void>) {
            return ast;
        }
    };

    template <>
    struct Combiner<void, void> {
        using Ast = void;

        Storable<void> combine(Storable<void>, Storable<void>) {
            return {};
        }
    };
}

#endif
