#include <filesystem.hpp>
#include <iostream>
#include <fstream>

int main(int argc, char **argv) {
    std::vector<std::filesystem::path> input_paths { argv + 1, argv + argc };

    std::unique_ptr<exo::Ast const> input_ast;
    for (auto const& input_path: input_paths) {
        auto&& input_path_ast { exo::parse_path(input_path) };
        if (!input_ast) input_ast = std::move(input_path_ast);
        else input_ast = std::make_unique<exo::InlineExpressionAst>(
            std::move(input_ast),
            std::move(input_path_ast));
    }

    std::ofstream output_file { "a.ast", std::ios::binary };
    output_file << input_ast->format();
}
