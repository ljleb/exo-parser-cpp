#ifndef AST_HPP
#define AST_HPP

#include <string>

namespace exo {
    struct Ast {
        virtual ~Ast() = default;
        virtual std::string format() const = 0;
    };
}

#endif
