#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP

#include <ast/expressions/inlineExpressionAst.hpp>
#include <ast/expressions/objectExpressionAst.hpp>
#include <ast/expressions/stringLiteralExpressionAst.hpp>

#include <parsers/expressionParser.hpp>

#include <filesystem>
#include <fstream>
#include <sstream>
#include <iostream>

namespace exo {
    inline exo::ExpressionParser::Ast parse_or_exit(exo::ExpressionParser::Input const& input) {
        auto&& parser_output { exo::root_expression_parser.try_parse(input) };

        if (parser_output.isLeft)
        {
            std::cerr << parser_output.leftValue << std::endl;
            exit(1);
        }

        return std::move(parser_output.rightValue.ast);
    }

    std::string read_file(std::string const& file_name) {
        std::ifstream in { file_name };
        std::ostringstream sstr;
        sstr << in.rdbuf();
        return sstr.str();
    }

    ExpressionParser::Ast parse_path(std::filesystem::path const& file) {
        if (std::filesystem::is_directory(file)) {
            std::vector<std::unique_ptr<Ast const>> properties;
            std::unique_ptr<Ast const> inline_property;

            for (std::filesystem::directory_entry const& directory_entry: std::filesystem::directory_iterator { file.string() }) {
                auto&& property_value { parse_path(directory_entry.path()) };
                if (directory_entry.path().filename() == ".xo") {
                    inline_property = std::move(property_value);
                } else {
                    static std::vector<std::unique_ptr<std::string>> path_property_names_owner;
                    path_property_names_owner.emplace_back(new std::string { std::move(directory_entry.path().filename().stem().string()) });
                    properties.emplace_back(std::make_unique<SymbolicPropertyAst>(
                        std::string_view { *path_property_names_owner.back() },
                        std::move(property_value)));
                }
            }

            std::unique_ptr<Ast>&& underlying_expression { std::make_unique<ObjectExpressionAst>(std::move(properties)) };

            if (inline_property) underlying_expression =
                std::make_unique<InlineExpressionAst>(
                    std::move(underlying_expression),
                    std::move(inline_property));

            return underlying_expression;
        } else if (file.string().ends_with(".xo")) {
            static std::vector<std::unique_ptr<std::string>> file_contents_owner;
            auto&& file_contents { read_file(file.string()) };
            file_contents_owner.emplace_back(new std::string { std::move(file_contents) });

            Source const source { *file_contents_owner.back() };
            ExpressionParser::Input const input { source };

            return parse_or_exit(input);
        } else {
            auto&& file_contents { read_file(file.string()) };
            return std::make_unique<StringLiteralExpressionAst>(std::move(file_contents));
        }
    }
}

#endif
