#ifndef EXO_SYMBOLIC_SELECTOR_AST_HPP
#define EXO_SYMBOLIC_SELECTOR_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <string_view>

namespace exo {
    struct SymbolicSelectorAst final : public Ast {
        SymbolicSelectorAst(std::string_view&& name):
            name { std::move(name) }
        {}

        virtual std::string format() const final override {
            return format::selector(format::symbol(format::literal(name)));
        }

    private:
        std::string_view name;
    };
}

#endif
