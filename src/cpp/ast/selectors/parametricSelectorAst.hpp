#ifndef EXO_PARAMETRIC_SELECTOR_AST_HPP
#define EXO_PARAMETRIC_SELECTOR_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <memory>

namespace exo {
    struct ParametricSelectorAst final : public Ast {
        ParametricSelectorAst(std::unique_ptr<Ast const>&& argument):
            argument { std::move(argument) }
        {}

        virtual std::string format() const final override {
            return format::selector(format::parameter(argument->format()));
        }

    private:
        std::unique_ptr<Ast const> argument;
    };
}

#endif
