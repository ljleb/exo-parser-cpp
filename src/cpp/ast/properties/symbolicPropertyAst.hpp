#ifndef EXO_SYMBOLIC_PROPERTY_AST_HPP
#define EXO_SYMBOLIC_PROPERTY_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <string_view>
#include <memory>

namespace exo {
    struct SymbolicPropertyAst final : public Ast {
        SymbolicPropertyAst(std::string_view&& name, std::unique_ptr<Ast const>&& value):
            name { std::move(name) },
            value { std::move(value) }
        {}

        virtual std::string format() const final override {
            return format::binding(
                format::identifier(format::symbol(format::literal(name))),
                value->format());
        }

    private:
        std::string_view name;
        std::unique_ptr<Ast const> value;
    };
}

#endif
