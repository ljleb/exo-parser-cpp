#ifndef EXO_PARAMETRIC_PROPERTY_AST_HPP
#define EXO_PARAMETRIC_PROPERTY_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <memory>

namespace exo {
    struct ParametricPropertyAst final : public Ast {
        ParametricPropertyAst(std::string_view&& parameter_name, std::unique_ptr<Ast const>&& value):
            parameter_name { std::move(parameter_name) },
            value { std::move(value) }
        {}

        virtual std::string format() const final override {
            return format::binding(
                format::identifier(format::parameter(format::symbol(format::literal(parameter_name)))),
                value->format());
        }

    private:
        std::string_view parameter_name;
        std::unique_ptr<Ast const> value;
    };
}

#endif
