#ifndef EXO_AST_FORMAT_HPP
#define EXO_AST_FORMAT_HPP

#include <vector>
#include <string>
#include <string_view>
#include <array>

namespace exo::format {
    std::string dispatch(std::string_view const name, std::string_view const data) {
        std::string result { name };
        result += ".";
        result += data;
        return result;
    }

    template <typename Data>
    std::string list(Data const& data) {
        if (data.empty()) return "[]";

        std::string result { "[" };
        auto it = data.cbegin();
        result += *it++;

        for (; it != data.cend(); ++it) {
            result += ",";
            result += *it;
        }

        result += "]";
        return result;
    }

    std::string binding(std::string_view const actor, std::string_view const target) {
        std::string result { "(" };
        result += actor;
        result += "|";
        result += target;
        result += ")";
        return result;
    }

    std::string selector(std::string_view const& selector) {
        return dispatch("selector", selector);
    }

    std::string identifier(std::string_view const& identifier) {
        return dispatch("identifier", identifier);
    }

    std::string context(std::string_view const& context) {
        return dispatch("context", context);
    }

    std::string initializer(std::string_view const& property) {
        return dispatch("initializer", property);
    }

    std::string parameter(std::string_view const& data) {
        return dispatch("parameter", data);
    }

    std::string symbol(std::string_view const name) {
        return dispatch("symbol", name);
    }

    std::string string(std::string_view const& data) {
        return dispatch("string", data);
    }

    std::string literal(std::string_view const body) {
        std::string result { "\"" };
        result += body;
        result += "\"";
        return result;
    }

    std::string print(std::string_view const data) {
        return dispatch("print", dispatch("output", data));
    }

    std::string print_error(std::string_view const data) {
        return dispatch("print", dispatch("error", data));
    }

    std::string cli_args() {
        return dispatch("cli", dispatch("args", list(std::array<std::string, 0> {})));
    }
}

#endif
