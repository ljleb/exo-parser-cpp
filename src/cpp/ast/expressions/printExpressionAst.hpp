#ifndef EXO_PRINT_EXPRESSION_AST_HPP
#define EXO_PRINT_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <memory>

namespace exo {
    enum class PrintStream {
        OUTPUT = 0,
        ERROR = 1,
    };

    template <PrintStream print_stream>
    struct PrintExpressionAst final : public Ast {
        PrintExpressionAst(std::unique_ptr<Ast const>&& expression):
            _expression { std::move(expression) }
        {}

        virtual std::string format() const final override {
            constexpr auto const format_print = print_stream == PrintStream::OUTPUT
                ? format::print
                : format::print_error;

            return format_print(_expression->format());
        }

    private:
        std::unique_ptr<Ast const> _expression;
    };
}

#endif
