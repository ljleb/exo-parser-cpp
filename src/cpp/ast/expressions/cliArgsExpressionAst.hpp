#ifndef EXO_CLI_ARGS_EXPRESSION_AST_HPP
#define EXO_CLI_ARGS_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

namespace exo {
    struct CliArgsExpressionAst final : public Ast {
        CliArgsExpressionAst()
        {}

        virtual std::string format() const final override {
            return format::cli_args();
        }
    };
}

#endif
