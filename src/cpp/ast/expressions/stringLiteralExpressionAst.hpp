#ifndef EXO_STRING_LITERAL_EXPRESSION_AST_HPP
#define EXO_STRING_LITERAL_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <string>

namespace exo {
    struct StringLiteralExpressionAst final : public Ast {
        StringLiteralExpressionAst(std::string&& content):
            content { std::move(content) }
        {}

        virtual std::string format() const final override {
            return format::string(format::literal(content));
        }

    private:
        std::string content;
    };
}

#endif
