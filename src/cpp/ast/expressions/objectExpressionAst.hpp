#ifndef EXO_OBJECT_EXPRESSION_AST_HPP
#define EXO_OBJECT_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <vector>

namespace exo {
    struct ObjectExpressionAst final : public Ast {
        ObjectExpressionAst(std::vector<std::unique_ptr<Ast const>>&& properties):
            properties { std::move(properties) }
        {}

        virtual std::string format() const final override {
            std::vector<std::string> formatted_properties;

            for (const auto& property: properties) {
                formatted_properties.push_back(property->format());
            }

            return format::initializer(format::list(formatted_properties));
        }

    private:
        std::vector<std::unique_ptr<Ast const>> properties;
    };
}

#endif
