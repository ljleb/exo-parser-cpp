#ifndef EXO_SELECTION_EXPRESSION_AST_HPP
#define EXO_SELECTION_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <vector>

namespace exo {
    struct SelectionExpressionAst final : public Ast {
        SelectionExpressionAst(
            std::unique_ptr<Ast const>&& initial_expression,
            std::vector<std::unique_ptr<Ast const>>&& selectors
        ):
            initial_expression { std::move(initial_expression) },
            selectors { std::move(selectors) }
        {}

        virtual std::string format() const final override {
            std::string formatted_selectors { initial_expression->format() };

            for (const auto& selector: selectors) {
                formatted_selectors = format::binding(selector->format(), formatted_selectors);
            }

            return formatted_selectors;
        }

    private:
        std::unique_ptr<Ast const> initial_expression;
        std::vector<std::unique_ptr<Ast const>> selectors;
    };
}

#endif
