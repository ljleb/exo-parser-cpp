#ifndef EXO_SYMBOL_EXPRESSION_AST_HPP
#define EXO_SYMBOL_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <string_view>

namespace exo {
    struct SymbolExpressionAst final : public Ast {
        SymbolExpressionAst(std::string_view&& name):
            name { std::move(name) }
        {}

        virtual std::string format() const final override {
            return format::symbol(format::literal(name));
        }

    private:
        std::string_view name;
    };
}

#endif
