#ifndef EXO_INLINE_EXPRESSION_AST_HPP
#define EXO_INLINE_EXPRESSION_AST_HPP

#include <ast.hpp>
#include <ast/format.hpp>

#include <memory>

namespace exo {
    struct InlineExpressionAst final : public Ast {
        InlineExpressionAst(
            std::unique_ptr<Ast const>&& context_expression,
            std::unique_ptr<Ast const>&& default_expression
        ):
            context_expression { std::move(context_expression) },
            default_expression { std::move(default_expression) }
        {}

        virtual std::string format() const final override {
            return format::binding(
                format::context(context_expression->format()),
                default_expression->format());
        }

    private:
        std::unique_ptr<Ast const> context_expression;
        std::unique_ptr<Ast const> default_expression;
    };
}

#endif
