FROM alpine:3.14 AS builder

ARG LINUX_AMD64_BINARY=out/release/main
COPY ${LINUX_AMD64_BINARY} /usr/local/bin/xo-ast
RUN chmod +x /usr/local/bin/xo-ast

FROM scratch

COPY --from=builder /usr/local/bin/xo-ast /usr/local/bin/xo-ast

ENTRYPOINT ["xo-ast"]
